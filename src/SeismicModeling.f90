! ============================================================================
! Name        : SeismicModeling.f90
! Author      : 
! Version     :
! Copyright   : Your copyright notice
! Description : Program to simulate seismic wave propagation in 3D acoustic
!               media.
! ============================================================================
program SeismicModeling

use MPICommunication
use WaveEquation
use IO

implicit none

integer         :: ierr, threads
real*8    		:: starttime, endtime

call MPIInitialize
call OMP_Set_Nested(.true.)
call OMP_Set_Dynamic(.false.)

threads = OMP_Get_Max_Threads()

#ifdef DEBUG
    call InitializeLog(procId)

    write(noutLog,'(//,a,i5)') 'Number of processors: ', OMP_Get_num_procs()

    !$OMP PARALLEL
    if (OMP_Get_thread_num() == 0) then
        write(noutLog,'(a,i5)') 'default number of threads: ', OMP_Get_num_threads()
    endif
    !$OMP END PARALLEL
#endif

call LoadParemeters(nx, ny, nz, h, dt, ntotal, fcutoff, kobs)

call InitializeParameters()

call CreateParallelGrid3D(nx, ny, nz)

call AllocateModelingArrays(nxlocal, nylocal, nzlocal)

call AllocateCommunicationArrays(HALF_LENGTH)

call LoadVelocityModel(nx, ny, nz, vel)

call FindProcWithObserver(planex, planey, kobs)

call FindProcWithSource(xsource, ysource, zsource)

if (procId == psource) call GenerateSeismicSource()

!if (procId == 0) then
!    call ClearSeismogramFile(SEISM_FILE_X)
!    call ClearSeismogramFile(SEISM_FILE_Y)
!endif

starttime = MPI_Wtime()

call InitializeModelingArrays(nxlocal, nylocal, nzlocal)


!------ compute pressure
call WavePropagation()


call SaveSeismogram(seismogramx, seismogramy, ntotal)

! grava snapshot

call mpi_barrier(mpi_comm_world, ierr)

endtime = MPI_Wtime()
if (procId == 0) write(*,*) 'Total time: ', endtime - starttime

call DeallocateModelingArrays()

call DeallocateCommunicationArrays()

if (procId == psource) call DeallocateSourceArray()

call DeallocateGridArrays()

#ifdef DEBUG
    close(noutLog)
#endif

! shut down MPI
call MPI_Finalize(ierr)



contains

! ============================================================================
! Subroutine    : WavePropagation
!
! Description   : Processes all time step.
! ============================================================================
subroutine WavePropagation()

integer         :: n, xi, yi, zi, step = 1
integer         :: xf, yf, zf

! Compute 1st time step outside loop in order to avoid unnecessary communication.
if (procId == psource) call ComputeSeismicSource(step, pnext, xpsource, ypsource, zpsource)

call DefineIndexesToComputeSubdomain(xi, xf, yi, yf, zi, zf, nx, ny, nz, HALF_LENGTH, .true.)

call ComputeWaveEquation(xi, xf, yi, yf, zi, zf, pprev, pnext, threads)

call BoundaryConditions(pprev, pnext)

call GenerateSeismogram(pnext, step)

step = step + 1

call DefineIndexesToComputeMsgRecv(nx, ny, nz, HALF_LENGTH)

call DefineIndexesToComputeSubdomain(xi, xf, yi, yf, zi, zf, nx, ny, nz, HALF_LENGTH, numProc == 1)

do n = step, ntotal-1, 2

    call ComputePropagation(xi, xf, yi, yf, zi, zf, step, pnext, pprev)

    step = step + 1

    call ComputePropagation(xi, xf, yi, yf, zi, zf, step, pprev, pnext)

    step = step + 1

enddo ! loop temporal

call ComputePropagation(xi, xf, yi, yf, zi, zf, step, pnext, pprev)

end subroutine WavePropagation



! ============================================================================
! Subroutine    : ComputePropagation
!
! Description   : Compute a single time step of wave propagation.
! ============================================================================
subroutine ComputePropagation(xi, xf, yi, yf, zi, zf, step, prev, next)

integer                                                 :: step, xi, xf, yi
integer                                                 :: yf, zi, zf, error
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: prev, next

call SendRecvMsgToNeighbors(prev, HALF_LENGTH, lower, upperx, uppery, upperz)

if (procId == psource) call ComputeSeismicSource(step, next, xpsource, ypsource, zpsource)

!$OMP PARALLEL DEFAULT(SHARED) NUM_THREADS(2)
if (OMP_Get_Thread_Num() == 1) then
    call ComputeWaveEquation(xi, xf, yi, yf, zi, zf, prev, next, threads-1)
else
    call MPI_Waitall(countRequest, request(1:countRequest), status(:,1:countRequest), error)
endif
!$OMP END PARALLEL

call UnpackMsgReceived(prev, HALF_LENGTH, lower, upperx, uppery, upperz)

call ComputeMsgReceived(prev, next, MAX_NEIGHBORS, indexes)

call BoundaryConditions(prev, next)

call GenerateSeismogram(next, step)

! gera snapshot

end subroutine ComputePropagation



! ============================================================================
! Subroutine    : GenerateSeismogram
!
! Description   :
! ============================================================================
subroutine GenerateSeismogram(pcurrent, n)

integer                                                 :: n, p, i, j
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: pcurrent

do p = 1, npx
    if (procMatrix(p,jpobs,kpobs) == procId) then
        do i = 1, nxlocal
            seismogramx(n,i) = pcurrent(i,ypobs,zpobs)
        enddo
    endif
enddo

do p = 1, npy
    if (procMatrix(ipobs,p,kpobs) == procId) then
        do j = 1, nylocal
            seismogramy(n,j) = pcurrent(xpobs,j,zpobs)
        enddo
    endif
enddo

end subroutine GenerateSeismogram



! ============================================================================
! Subroutine    : BoundaryConditions
!
! Description   : Implements Reynolds nonreflecting boundary condition. This
!                 boundary condition greatly reduces reflections from the edge
!                 of the model.
! ============================================================================
subroutine BoundaryConditions(prev, next)

integer                                                 :: xii, xff, yii, yff, zii, zff
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: prev, next

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(xii,xff,yii,yff,zii,zff)
xii = 1
xff = nxlocal
yii = 1
yff = nylocal
zii = 1
zff = nzlocal

if (neighbors(LEFT) == -1) xii = HALF_LENGTH + 1

if (neighbors(RIGHT) == -1) xff = nxlocal - HALF_LENGTH

if (neighbors(FRONT) == -1) yff = nylocal - HALF_LENGTH

if (neighbors(BACK) == -1) yii = HALF_LENGTH + 1

if (neighbors(BOTTOM) == -1) zff = nzlocal - HALF_LENGTH

if (neighbors(TOP) == -1) zii = HALF_LENGTH + 1

if (neighbors(LEFT) == -1) then
    call LeftBoundary(prev, next, yii, yff, zii, zff)
endif

if (neighbors(RIGHT) == -1) then
    call RightBoundary(prev, next, nxlocal, yii, yff, zii, zff)
endif

if (neighbors(FRONT) == -1) then
    call FrontBoundary(prev, next, nylocal, xii, xff, zii, zff)
endif

if (neighbors(BACK) == -1) then
    call BackBoundary(prev, next, xii, xff, zii, zff)
endif

if (neighbors(BOTTOM) == -1) then
    call BottomBoundary(prev, next, nzlocal, xii, xff, yii, yff)
endif

!if (neighbors(TOP) == -1) then
!    call TopBoundary(prev, next, xii, xff, yii, yff)
!endif

!$OMP END PARALLEL

end subroutine BoundaryConditions



end program


