module GlobalDataTypes

implicit none

type Neighborhood
    integer     :: top, bottom, front, back, left, right
end type Neighborhood


! ============================================================================
! Data type     : StartEndIndex
!
! Description   : Define start and end indexes to compute message received
!                 from each neighbor.
! ============================================================================
type StartEndIndex
    integer     :: xi, xf, yi, yf, zi, zf
end type StartEndIndex

end module GlobalDataTypes
