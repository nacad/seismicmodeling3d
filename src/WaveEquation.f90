! ============================================================================
! Name        : WaveEquation.f90
! Author      : Schirley Corrêa Jorge
! Description : Contains variables and subroutines needed to calculate wave
!               equation. The propagation will be implemented with a 8th order
!               in space and 2nd order in time.
! ============================================================================
module WaveEquation

use GlobalDataTypes
use IO
use omp_lib

implicit none
integer, parameter                  :: HALF_LENGTH = 4          ! Half length of space order
integer                             :: nx, ny, nz               ! model dimension
integer                             :: ntotal                   ! total of time steps
integer                             :: kobs                     ! depth of the observer
integer                             :: xsource,ysource,zsource  ! seismic source coordinates(x, y, z)
integer                             :: ns                       ! number of seismic source time steps
integer                             :: planex, planey           ! planes used to generate seismogram
integer                             :: lower, upperx, uppery, upperz ! lower and upper bound for pressure arrays

real                                :: h                        ! mesh spacing for FDM (finite difference method)
real                                :: dt                       ! time interval
real                                :: fcutoff                  ! cutoff frequency

real, dimension(:), allocatable     :: source                   ! seismic source

real, dimension(:,:), allocatable   :: seismogramx              ! seismogram generated along of x
real, dimension(:,:), allocatable   :: seismogramy              ! seismogram generated along of y

real, dimension(:,:,:), allocatable :: pprev, pnext             ! pressure at times t-1, t+1, respectively
real, dimension(:,:,:), allocatable :: vel                      ! velocity model
real, dimension(:,:,:), allocatable :: c                        ! auxiliary variable to compute wave equation

real, parameter                     :: c0 =  -205./72., c1 = 8./5.
real, parameter                     :: c2 = -1./5., c3 = 8./315., c4 = -1./560.



contains
! ============================================================================
! Subroutine    : InitializeParameters
!
! Description   : Initialize parameters for seismic source and seismogram.
! ============================================================================
subroutine InitializeParameters()

xsource = nx/2
ysource = ny/2
zsource = kobs
planex = nx/2
planey = ny/2

end subroutine InitializeParameters



! ============================================================================
! Subroutine    : AllocateArrays
!
! Description   : Allocate all arrays needed to compute wave equation.
! ============================================================================
subroutine AllocateModelingArrays(nxlocal, nylocal, nzlocal)

integer         :: nxlocal, nylocal, nzlocal
integer         :: start, endx, endy, endz, status

lower = 1 - HALF_LENGTH
upperx = nxlocal + HALF_LENGTH
uppery = nylocal + HALF_LENGTH
upperz = nzlocal + HALF_LENGTH

allocate(pprev(lower:upperx, lower:uppery, lower:upperz), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate pressure previous array ***'

allocate(pnext(lower:upperx, lower:uppery, lower:upperz), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate pressure next array ***'

allocate(vel(nxlocal, nylocal, nzlocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate velocity array ***'

allocate(c(nxlocal, nylocal, nzlocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate auxiliary C array ***'

allocate(seismogramx(ntotal, nxlocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate seismogramX array ***'

allocate(seismogramy(ntotal, nylocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate seismogramY array ***'

#ifdef DEBUG
    call PrintArrayAllocation(lower, upperx, uppery, upperz, nxlocal, nylocal, nzlocal, ntotal)
#endif

end subroutine AllocateModelingArrays



! ============================================================================
! Subroutine    : DeallocateArrays
!
! Description   : Deallocate all arrays needed to compute wave equation.
! ============================================================================
subroutine DeallocateModelingArrays()

integer     :: status

deallocate(pprev, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate pressure previous array ***'

deallocate(pnext, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate pressure next array ***'

deallocate(vel, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate velocity array ***'

deallocate(c, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate auxiliary C array ***'

deallocate(seismogramx, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate seismogramX array ***'

deallocate(seismogramy, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate seismogramY array ***'

end subroutine DeallocateModelingArrays



! ============================================================================
! Subroutine    : GenerateSeismicSource
!
! Description   : Generate seismic source using gaussian fuction.
! ============================================================================
subroutine GenerateSeismicSource()

integer         :: n, status

real            :: Tf   ! Gaussian period
real            :: fc   ! central frequence
real            :: t    ! time, shoud be positive

real, parameter :: PI = 3.141593

ns = 4 * sqrt(PI)/(fcutoff * dt)
Tf = 2*sqrt(PI)/fcutoff
fc = fcutoff/(3.*sqrt(PI))

allocate(source(ns+1), stat=status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate seismic source array ***'


do n = 1, ns+1
    t = (n-1) * dt-Tf
    source(n)=-exp(-PI*(PI*fc*t)*(PI*fc*t))*(1.-2.*PI*(PI*fc*t)*(PI*fc*t))
enddo


#ifdef DEBUG
    write(noutLog,'(//,a)') 'Info Seismic source:'
    write(noutLog,*) 'ns: ', ns
    write(noutLog,*) 'Tf = ', Tf
    write(noutLog,*) 'fc = ', fc
#endif

end subroutine GenerateSeismicSource



! ============================================================================
! Subroutine    : DeallocateSourceArray
!
! Description   : Deallocate seismic source array.
! ============================================================================
subroutine DeallocateSourceArray()

integer         status

deallocate(source, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate seismic source array ***'

end subroutine DeallocateSourceArray



! ============================================================================
! Subroutine    : InitializeModelingArrays
!
! Description   : Initialize arrays for velocity model, pressure and
!                 auxiliaries.
! ============================================================================
subroutine InitializeModelingArrays(nxlocal, nylocal, nzlocal)

integer         :: nxlocal, nylocal, nzlocal, i, j, k
!integer         :: start, endx, endy, endz
!
!start = 1 - HALF_LENGTH
!endx = nxlocal + HALF_LENGTH
!endy = nylocal + HALF_LENGTH
!endz = nzlocal + HALF_LENGTH

do k = 1, nzlocal
    do j = 1, nylocal
        do i = 1, nxlocal
            vel(i,j,k) = vel(i,j,k) * dt/h
            c(i,j,k) = vel(i,j,k)*vel(i,j,k)
        enddo
    enddo
enddo

!do i = start, endx
!    do j = start, endy
!        do k = start, endz
!            pprev(k,j,i) = 0
!            pnext(k,j,i)= 0
!        enddo
!    enddo
!enddo

pprev = 0.
pnext = 0.
seismogramx = 0.
seismogramy = 0.

end subroutine InitializeModelingArrays



! ============================================================================
! Subroutine    : ComputeSeismicSource
!
! Description   : Add seismic source to compute pressure field
! ============================================================================
subroutine ComputeSeismicSource(step, next, xpsrc, ypsrc, zpsrc)

integer                                                 :: step, xpsrc, ypsrc, zpsrc
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: next

if (step <= ns+1) then
    next(xpsrc, ypsrc, zpsrc) = next(xpsrc, ypsrc, zpsrc) + source(step)
endif

end subroutine ComputeSeismicSource



! ============================================================================
! Subroutine    : ComputeWaveEquation
!
! Description   :
! ============================================================================
subroutine ComputeWaveEquation(startx, endx, starty, endy, startz, endz, prev, next, threads)

integer                                                 :: startx, starty, startz
integer                                                 :: endx, endy, endz, nxx, nyy
integer                                                 :: i, j, k, threads, ii
real, dimension(lower:upperx,lower:uppery,lower:upperz),target :: prev, next
real, dimension(:), pointer                                    :: ptprev, ptnext

#ifdef DEBUG
    write(noutLog,'(//,a)') 'Computing Wave Equation:'
    write(noutLog,*) 'startx: ', startx
    write(noutLog,*) 'endx: ', endx
    write(noutLog,*) 'starty: ', starty
    write(noutLog,*) 'endy: ', endy
    write(noutLog,*) 'startz: ', startz
    write(noutLog,*) 'endz: ', endz
#endif

nxx = upperx + HALF_LENGTH
nyy = uppery + HALF_LENGTH

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,ii,ptprev,ptnext) NUM_THREADS(threads)
!$OMP DO SCHEDULE(DYNAMIC)
do k = startz, endz
    do j = starty, endy
        ptprev => prev(:,j,k)
        ptnext => next(:,j,k)
        ii = startx
        do i = startx+HALF_LENGTH, endx+HALF_LENGTH
            ptnext(i) = 2. * ptprev(i) - ptnext(i) + &
                c(ii,j,k) * (3. * c0 * ptprev(i) + &
                c1 * (ptprev(i-1) + ptprev(i+1) + ptprev(i-nxx) + ptprev(i+nxx) + ptprev(i-nxx*nyy) + ptprev(i+nxx*nyy)) + &
                c2 * (ptprev(i-2) + ptprev(i+2) + ptprev(i-2*nxx) + ptprev(i+2*nxx) + ptprev(i-2*nxx*nyy) + ptprev(i+2*nxx*nyy)) + &
                c3 * (ptprev(i-3) + ptprev(i+3) + ptprev(i-3*nxx) + ptprev(i+3*nxx) + ptprev(i-3*nxx*nyy) + ptprev(i+3*nxx*nyy)) + &
                c4 * (ptprev(i-4) + ptprev(i+4) + ptprev(i-4*nxx) + ptprev(i+4*nxx) + ptprev(i-4*nxx*nyy) + ptprev(i+4*nxx*nyy)))

            ii = ii + 1
        enddo
    enddo
enddo
!$OMP END DO NOWAIT
!$OMP END PARALLEL

end subroutine ComputeWaveEquation



! ============================================================================
! Subroutine    : ComputeWaveEquation4MsgRecv
!
! Description   :
! ============================================================================
subroutine ComputeMsgReceived(prev, next, neighbors, indexes)

integer                                                 :: neighbors, i, threads
integer                                                 :: xi, xf, yi, yf, zi, zf
type (StartEndIndex)                                    :: indexes(neighbors)
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: prev, next

threads = OMP_Get_Max_Threads()
! criar uma região paralela aqui

do i = 1, neighbors
    if (indexes(i)%xi /= -1) then
        xi = indexes(i)%xi
        xf = indexes(i)%xf
        yi = indexes(i)%yi
        yf = indexes(i)%yf
        zi = indexes(i)%zi
        zf = indexes(i)%zf
        call ComputeWaveEquation(xi, xf, yi, yf, zi, zf, prev, next, threads)
    endif
enddo

end subroutine ComputeMsgReceived



! ============================================================================
! Subroutine    : LeftBoundary
!
! Description   : Applies Reynolds nonreflecting boundary condition to left
!                 boundary.
! ============================================================================
subroutine LeftBoundary(prev, next, yi, yf, zi, zf)

integer                                                 :: i, j, k, yi, yf, zi, zf
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: prev, next

!$OMP DO SCHEDULE(DYNAMIC)
do k = zi, zf
    do j = yi, yf
        do i = 1, HALF_LENGTH
            next(i,j,k) = prev(i,j,k) + vel(i,j,k) * ( prev(i+1,j,k) - prev(i,j,k) )
        enddo
    enddo
enddo
!$OMP END DO NOWAIT

end subroutine LeftBoundary



! ============================================================================
! Subroutine    : RightBoundary
!
! Description   : Applies Reynolds nonreflecting boundary condition to right
!                 boundary.
! ============================================================================
subroutine RightBoundary(prev, next, nxlocal, yi, yf, zi, zf)

integer                                                 :: i, j, k, yi, yf, zi, zf
integer                                                 :: nxlocal
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: prev, next

!$OMP DO SCHEDULE(DYNAMIC)
do k = zi, zf
    do j = yi, yf
        do i = (nxlocal - HALF_LENGTH) + 1, nxlocal
            next(i,j,k) = prev(i,j,k) - vel(i,j,k) * ( prev(i,j,k) - prev(i-1,j,k) )
        enddo
    enddo
enddo
!$OMP END DO NOWAIT

end subroutine RightBoundary



! ============================================================================
! Subroutine    : FrontBoundary
!
! Description   : Applies Reynolds nonreflecting boundary condition to front
!                 boundary.
! ============================================================================
subroutine FrontBoundary(prev, next, nylocal, xi, xf, zi, zf)

integer                                                 :: i, j, k, xi, xf, zi, zf
integer                                                 :: nylocal
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: prev, next

!$OMP DO SCHEDULE(DYNAMIC)
do k = zi, zf
    do j = (nylocal - HALF_LENGTH) + 1, nylocal
        do i = xi, xf
            next(i,j,k) = prev(i,j,k) - vel(i,j,k) * ( prev(i,j,k) - prev(i,j-1,k) )
        enddo
    enddo
enddo
!$OMP END DO NOWAIT

end subroutine FrontBoundary



! ============================================================================
! Subroutine    : BackBoundary
!
! Description   : Applies Reynolds nonreflecting boundary condition to back
!                 boundary.
! ============================================================================
subroutine BackBoundary(prev, next, xi, xf, zi, zf)

integer                                                 :: i, j, k, xi, xf, zi, zf
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: prev, next

!$OMP DO SCHEDULE(DYNAMIC)
do k = zi, zf
    do j = 1, HALF_LENGTH
        do i = xi, xf
            next(i,j,k) = prev(i,j,k) + vel(i,j,k) * ( prev(i,j+1,k) - prev(i,j,k) )
        enddo
    enddo
enddo
!$OMP END DO NOWAIT

end subroutine BackBoundary



! ============================================================================
! Subroutine    : BottomBoundary
!
! Description   : Applies Reynolds nonreflecting boundary condition to bottom
!                 boundary.
! ============================================================================
subroutine BottomBoundary(prev, next, nzlocal, xi, xf, yi, yf)

integer                                                 :: i, j, k, xi, xf, yi, yf
integer                                                 :: nzlocal
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: prev, next

!$OMP DO SCHEDULE(DYNAMIC)
do k = (nzlocal - HALF_LENGTH) + 1, nzlocal
    do j = yi, yf
        do i = xi, xf
            next(i,j,k) = prev(i,j,k) - vel(i,j,k) * ( prev(i,j,k) - prev(i,j,k-1) )
        enddo
    enddo
enddo
!$OMP END DO NOWAIT

end subroutine BottomBoundary



! ============================================================================
! Subroutine    : TopBoundary
!
! Description   : Applies Reynolds nonreflecting boundary condition to top
!                 boundary.
! ============================================================================
subroutine TopBoundary(prev, next, xi, xf, yi, yf)

integer                                                 :: i, j, k, xi, xf, yi, yf
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: prev, next

!$OMP DO SCHEDULE(DYNAMIC)
do k = 1, HALF_LENGTH
    do j = yi, yf
        do i = xi, xf
            next(i,j,k) = prev(i,j,k) + vel(i,j,k) * ( prev(i,j,k+1) - prev(i,j,k) )
        enddo
    enddo
enddo
!$OMP END DO NOWAIT

end subroutine TopBoundary



end module WaveEquation
