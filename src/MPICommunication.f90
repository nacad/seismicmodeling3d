! ============================================================================
! Name        : MPICommunication.f90
! Author      : Schirley Corrêa Jorge
! Description : Management all MPI operations
! ============================================================================
module MPICommunication

use GlobalDataTypes
use mpi
use IO

implicit none

integer, parameter                  :: MAX_NEIGHBORS = 6
integer, parameter                  :: TOP = 1, BACK = 2, LEFT = 3, RIGHT = 4, FRONT = 5, BOTTOM = 6 ! neighbors positions

integer                             :: numProc                      ! total number of MPI processes
integer                             :: procId                       ! current MPI process
integer                             :: psource                      ! process that contains the seismic source
integer                             :: npx, npy, npz                ! number of processes in each direction
integer                             :: nxlocal, nylocal, nzlocal    ! local size of subdomain in x, y, z
integer                             :: startx, starty, startz       ! local star point in x, y, z
integer                             :: endx, endy, endz
integer                             :: xpsource, ypsource, zpsource ! coordinates x, y, z relative to psource subdomain

integer                             :: ipobs, jpobs, kpobs          ! Indexes of observer process related to proceMatrix
integer                             :: xpobs, ypobs, zpobs          ! coordinates x, y, z relative to observer process
integer                             :: countRequest                 ! Number of MPI Requests

integer, dimension(MAX_NEIGHBORS)   :: neighbors                    ! neighbors of each MPI processes
integer, dimension(2*MAX_NEIGHBORS) :: request                      ! MPI Request used to send and receive message
integer, dimension(MPI_STATUS_SIZE,2*MAX_NEIGHBORS) :: status       ! MPI Status
integer, dimension(:), allocatable  :: lnx, lny, lnz                ! list of size in x, y, z
integer, dimension(:), allocatable  :: lstartx,lstarty,lstartz      ! List of start point in x, y, z

integer, dimension(:,:,:), allocatable :: procMatrix                ! Matrix of processes

real, dimension(:,:,:), allocatable :: sendBuffTop, sendBuffBot     ! Buffer to send message
real, dimension(:,:,:), allocatable :: sendBuffBack, sendBuffFront
real, dimension(:,:,:), allocatable :: sendBuffRight, sendBuffLeft
real, dimension(:,:,:), allocatable :: recvBuffTop, recvBuffBot     ! Buffer to receive message
real, dimension(:,:,:), allocatable :: recvBuffBack, recvBuffFront
real, dimension(:,:,:), allocatable :: recvBuffRight, recvBuffLeft

type (StartEndIndex), allocatable   :: indexes(:)                   ! start/end indexes to compute message



contains
! ============================================================================
! Subroutine    : MPIInitialize
!
! Description   : Start up MPI
! ============================================================================
subroutine MPIInitialize

integer         :: ierror

call mpi_init(ierror)
call mpi_comm_size(mpi_comm_world, numProc, ierror)
call mpi_comm_rank(mpi_comm_world, procId, ierror)

end subroutine MPIInitialize



! ============================================================================
! Subroutine    : CreateParallelGrid3D
!
! Description   : Define how the MPI processes going to be allocated in each
!                 direction.
! ============================================================================
subroutine CreateParallelGrid3D(nx, ny, nz)

integer         :: nx, ny, nz
integer         :: i, aux1, aux2, aux3
real            :: tmp

aux2 = nx*nx
aux3 = nz*ny

aux1 = (aux2/aux3)*numProc
tmp = 1.0/3.0

npx = INT(aux1**tmp)

if(npx == 0) npx = 1
do while (npx > 0)
    aux1 = numProc/npx
    if(npx*aux1 == numProc) exit
    npx = npx - 1
end do
if(npx == 0) npx = 1

tmp = ny*numProc/(nz*npx)

npy = INT(SQRT(tmp))
if(npy == 0) npy = 1
do while (npy > 0)
    npz = numProc/(npx*npy)
    if(npx*npy*npz == numProc) exit
    npy = npy - 1
end do
if(npy == 0) npy = 1

if(nx > nz .AND. npx < npz) then
    aux1 = npz
    npz = npx
    npx = aux1
endif

if(npx*npy*npz /= numProc) stop 'Could not find good partition.'
if(nx < npx) stop 'Partition in x direction is greater than Nx'
if(ny < npy) stop 'Partition in y direction is greater than Ny'
if(nz < npz) stop 'Partition in z direction is greater than Nz'


! For each process, determine local size and grid start point in each direction

allocate(lnx(npx))
aux1 = MOD(nx,npx)
do i=1,npx
    aux2 = MOD(i,npx)
    lnx(i) = nx/npx
    if (aux2 > 0 .AND. aux1 >= aux2) lnx(i) = lnx(i) +1
enddo

aux1 = mod(procId, npx)
nxlocal = lnx(aux1+1)

allocate(lstartx(npx))
lstartx = 1
do i=2,npx
    lstartx(i) = lstartx(i-1) + lnx(i-1)
enddo
startx = lstartx(aux1+1)


allocate(lny(npy))
aux1 = MOD(ny,npy)
do i=1,npy
    aux2 = MOD(i,npy)
    lny(i) = ny/npy
    if (aux2 > 0 .AND. aux1 >= aux2) lny(i) = lny(i) + 1
enddo

aux1 = ((mod(procId, npx*npy))/npx)
nylocal = lny(aux1+1)

allocate(lstarty(npy))
lstarty = 1
do i=2,npy
    lstarty(i) = lstarty(i-1) + lny(i-1)
enddo
starty = lstarty(aux1+1)


allocate(lnz(npz))
aux1 = MOD(nz,npz)
do i=1,npz
    aux2 = MOD(i,npz)
    lnz(i) = nz/npz
    if (aux2 > 0 .and. aux1 >= aux2) lnz(i) = lnz(i) + 1
enddo

aux1 = (procId/(npx*npy))
nzlocal = lnz(aux1+1)

allocate(lstartz(npz))
lstartz = 1
do i=2,npz
lstartz(i) = lstartz(i-1) + lnz(i-1)
enddo
startz = lstartz(aux1+1)

!-------------------------------------------------
endx = startx + nxlocal -1
endy = starty + nylocal -1
endz = startz + nzlocal -1

call ComputeNeighbors(nx, ny, nz)

call CreateProcessesMatrix

#ifdef DEBUG
    call PrintGridInFile(nx, ny, nz)
#endif

end subroutine CreateParallelGrid3D



! ============================================================================
! Subroutine    : ComputeNeighbors
!
! Description   : Determine the neighborhood relationship among the processes.
! ============================================================================
subroutine ComputeNeighbors(nx, ny, nz)

integer         :: nx, ny, nz

neighbors(TOP) = procId - npx*npy       ! top neighbor
neighbors(BACK) = procId - npx          ! back neighbor
neighbors(LEFT) = procId - 1            ! left neighbor
neighbors(RIGHT) = procId + 1           ! right neighbor
neighbors(FRONT) = procId + npx         ! front neighbor
neighbors(BOTTOM) = procId + npx*npy    ! bottom neighbor

if (startx == 1) neighbors(LEFT)  = -1

if (endx == nx) neighbors(RIGHT)  = -1

if (starty==1) neighbors(BACK)  = -1

if (endy == ny) neighbors(FRONT)  = -1

if (startz == 1) neighbors(TOP) = -1

if (endz == nz) neighbors(BOTTOM) = -1

end subroutine ComputeNeighbors



! ============================================================================
! Subroutine    : CreateProcessesMatrix
!
! Description   :
! ============================================================================
subroutine CreateProcessesMatrix

integer         :: i, j, k, proc, status

allocate(procMatrix(npx, npy, npz), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate matrix of processes ***'

proc = 0
do k = 1, npz
    do j = 1, npy
        do i = 1, npx
            procMatrix(i,j,k) = proc
            proc = proc + 1
        enddo
    enddo
enddo


#ifdef DEBUG
    write(noutLog,'(//,a)') 'Matrix of processes:'
    do k = 1, npz
        do j = 1, npy
            do i = 1, npx
                write(noutLog,"('P(',i2,',',i2,',',i2,') = ',i2)") i,j,k,procMatrix(i,j,k)
            enddo
        enddo
    enddo
#endif

end subroutine CreateProcessesMatrix



! ============================================================================
! Subroutine    : AllocateBuffers
!
! Description   : Allocate buffers to exchange message among the MPI processes.
! ============================================================================
subroutine AllocateCommunicationArrays(halfLength)

integer                 :: halfLength, status

allocate(sendBuffTop(nxlocal, nylocal, halfLength), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate send buffer top ***'

allocate(sendBuffBot(nxlocal, nylocal, halfLength), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate send buffer bottom ***'

allocate(sendBuffBack(nxlocal, halfLength, nzlocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate send buffer back ***'

allocate(sendBuffFront(nxlocal, halfLength, nzlocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate send buffer front ***'

allocate(sendBuffLeft(halfLength, nylocal, nzlocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate send buffer left ***'

allocate(sendBuffRight(halfLength, nylocal, nzlocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate send buffer right ***'


allocate(recvBuffTop(nxlocal, nylocal, halfLength), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate recv buffer top ***'

allocate(recvBuffBot(nxlocal, nylocal, halfLength), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate recv buffer bottom ***'

allocate(recvBuffBack(nxlocal, halfLength, nzlocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate recv buffer back ***'

allocate(recvBuffFront(nxlocal, halfLength, nzlocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate recv buffer front ***'

allocate(recvBuffLeft(halfLength, nylocal, nzlocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate recv buffer left ***'

allocate(recvBuffRight(halfLength, nylocal, nzlocal), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate recv buffer right ***'

allocate(indexes(MAX_NEIGHBORS), stat = status)
if (status /= 0) write (*,*) '*** Not enough memory to allocate indexes array ***'

end subroutine AllocateCommunicationArrays



! ============================================================================
! Subroutine    : LoadVelocityModel
!
! Description   : Load velocity model and distribute among the MPI processes.
! ============================================================================
subroutine LoadVelocityModel(nx, ny, nz, vel)

integer                                 :: nx, ny, nz, status, buffsize, ierr,k

real, dimension(:,:,:)                  :: vel
real, dimension(:,:,:), allocatable     :: velGlobal

if(procId == 0) then
    allocate(velGlobal(nx, ny, nz),stat = status)
    if(status /= 0) write (*,*) '*** Not enough memory to allocate global velocity matrix ***'

    call ReadVelocityModel(nx, ny, nz, velGlobal)

    call DistributeVelocityModel(velGlobal)

    call CopyVelModelToSubarray(nxlocal, nylocal, nzlocal, startx, starty, startz, vel, velGlobal)

    deallocate(velGlobal,stat = status)

else
    buffsize = nxlocal * nylocal * nzlocal

    call MPI_RECV(vel, buffsize, MPI_REAL, 0, 1, MPI_COMM_WORLD, status, ierr)

end if

!#ifdef DEBUG
!    call PrintVelSubArray(vel,nxlocal, nylocal, nzlocal)
!#endif

end subroutine LoadVelocityModel



! ============================================================================
! Subroutine    : DistributeVelocityModel
!
! Description   : Distribute velocity model among the MPI processes.
! ============================================================================
subroutine DistributeVelocityModel(velGlobal)

integer                                 :: rank, aux,sizex, sizey, sizez, ierr
integer                                 :: strtx, strty, strtz, status, buffsize

real, dimension(:,:,:)                  :: velGlobal
real, dimension(:,:,:), allocatable     :: velSend

do rank = 1, numProc-1
    aux = mod(rank, npx)
    sizex = lnx(aux +1 )
    strtx = lstartx(aux + 1)

    aux = ((mod(rank, npx * npy))/npx)
    sizey = lny(aux + 1)
    strty = lstarty(aux + 1)

    aux = (rank/(npx * npy))
    sizez = lnz(aux + 1)
    strtz = lstartz(aux + 1)

    allocate(velSend(sizex, sizey, sizez), stat = status)
    if(status /= 0) write (*,*) '*** Not enough memory to allocate velocity matrix to send subdomain ***'

    call CopyVelModelToSubarray(sizex, sizey, sizez, strtx, strty, strtz, velSend, velGlobal)

    buffsize = sizex * sizey * sizez

    call MPI_SEND(velSend(1,1,1),buffsize,MPI_REAL,rank,1,MPI_COMM_WORLD,ierr)

    deallocate(velSend,stat = status)

enddo

end subroutine DistributeVelocityModel



! ============================================================================
! Subroutine    : CopyVelModelToSubarray
!
! Description   : Copy part of global velocity model to a subarray
!
! ============================================================================
subroutine CopyVelModelToSubarray(sizex, sizey, sizez, strtx, strty, strtz, velSend, velGlobal)

integer                                 :: sizex, sizey, sizez, i, j, k, x, y, z
integer                                 :: strtx, strty, strtz, edx, edy, edz
real,dimension(:,:,:)                   :: velGlobal
real,dimension(sizex,sizey,sizez)       :: velSend

edx = strtx + sizex - 1
edy = strty + sizey - 1
edz = strtz + sizez - 1

z = 1
do k=strtz, edz
    y = 1
    do j=strty, edy
        x = 1
        do i=strtx, edx
            velSend(x,y,z) = velGlobal(i,j,k)
            x = x + 1
        enddo
        y = y + 1
    enddo
    z = z + 1
enddo

end subroutine CopyVelModelToSubarray



! ============================================================================
! Subroutine    : FindProcWithSource
!
! Description   : Identify which MPI process contains the seismic source.
! ============================================================================
subroutine FindProcWithSource(xsource, ysource, zsource)

integer         :: xsource, ysource, zsource
integer         :: i, j, k, x, y, z

do i = 1, npx
    if(xsource < lstartx(i)) exit
enddo
i = i -1
xpsource = xsource - lstartx(i) + 1

do j = 1, npy
    if(ysource < lstarty(j)) exit
enddo
j = j -1
ypsource = ysource - lstarty(j) + 1

do k = 1, npz
    if(zsource < lstartz(k)) exit
enddo
k = k -1
zpsource = zsource - lstartz(k) + 1

y = 0
z = 0
if(k > 1) z = (k - 1) * npx * npy
if(j > 1) y = (j - 1) * npx

psource = (z + y + i) - 1

#ifdef DEBUG
  write(noutLog,'(//,a,i5)') 'Process with source: ', psource
  write(noutLog,*) 'xpsource: ', xpsource
  write(noutLog,*) 'ypsource: ', ypsource
  write(noutLog,*) 'zpsource: ', zpsource
#endif

end subroutine FindProcWithSource



! ============================================================================
! Subroutine    : PrintGridInFile
!
! Description   : Test whether the grid was created properly.
! ============================================================================
subroutine PrintGridInFile(nx, ny, nz)

integer         :: i, nx, ny, nz

write(noutLog,1000) procId
write(noutLog,1006) npx, npy, npz
write(noutLog,1001) nx,nxlocal
write(noutLog,1004) startx, endx
write(noutLog,1002) ny,nylocal
write(noutLog,1004) starty, endy
write(noutLog,1003) nz,nzlocal
write(noutLog,1004) startz, endz
write(noutLog,*)  ' Neighbors Processes: '
do i=1,MAX_NEIGHBORS
    write(noutLog,1005) i, neighbors(i)
enddo

1000 format (//' Grid Info (process ', i5,')')
1001 format (' # Points in x : ', i5, '  (global) /', i5, '(local)')
1002 format (' # Points in y : ', i5, '  (global) /', i5, '(local)')
1003 format (' # Points in z : ', i5, '  (global) /', i5, '(local)')
1004 format ('   start: ', i5,'  end: ', i5)
1005 format ('     Neighboor(',i2,')=',i4)
1006 format (' # processes in x, y, z:', i4, i4, i4)

end subroutine PrintGridInFile



! ============================================================================
! Subroutine    : DeallocateGridArrays
!
! Description   : Deallocate all arrays needed to compute wave equation.
! ============================================================================
subroutine DeallocateGridArrays()

integer     :: status

deallocate(lnx, stat = status)
if (status /= 0) write (*,*) '*** it is not possible deallocate size x array ***'

deallocate(lny, stat = status)
if (status /= 0) write (*,*) '*** it is not possible deallocate size y array ***'

deallocate(lnz, stat = status)
if (status /= 0) write (*,*) '*** it is not possible deallocate size z array ***'

deallocate(lstartx, stat = status)
if (status /= 0) write (*,*) '*** it is not possible deallocate start point x array ***'

deallocate(lstarty, stat = status)
if (status /= 0) write (*,*) '*** it is not possible deallocate start point y array ***'

deallocate(lstartz, stat = status)
if (status /= 0) write (*,*) '*** it is not possible deallocate start point z array ***'

end subroutine DeallocateGridArrays



! ============================================================================
! Subroutine    : SendRecvMsgToNeighbors
!
! Description   :
! ============================================================================
subroutine SendRecvMsgToNeighbors(pcurrent, halfLength, lower, upperx, uppery, upperz)

integer                                                 :: halfLength
integer                                                 :: lower, upperx, uppery, upperz
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: pcurrent

request = MPI_REQUEST_NULL
countRequest = 0

if (neighbors(TOP) /= -1) then
    call RecvMsgFromTop(neighbors(TOP), halfLength)
    call SendMsgToTop(pcurrent, halfLength, neighbors(TOP), lower, upperx, uppery, upperz)
endif

if (neighbors(BOTTOM) /= -1) then
    call RecvMsgFromBottom(neighbors(BOTTOM), halfLength)
    call SendMsgToBottom(pcurrent, halfLength, neighbors(BOTTOM), lower, upperx, uppery, upperz)
endif

if (neighbors(BACK) /= -1) then
    call RecvMsgFromBack(neighbors(BACK), halfLength)
    call SendMsgToBack(pcurrent, halfLength, neighbors(BACK), lower, upperx, uppery, upperz)
endif

if (neighbors(FRONT) /= -1) then
    call RecvMsgFromFront(neighbors(FRONT), halfLength)
    call SendMsgToFront(pcurrent, halfLength, neighbors(FRONT), lower, upperx, uppery, upperz)
endif

if (neighbors(LEFT) /= -1) then
    call RecvMsgFromLeft(neighbors(LEFT), halfLength)
    call SendMsgToLeft(pcurrent, halfLength, neighbors(LEFT), lower, upperx, uppery, upperz)
endif

if (neighbors(RIGHT) /= -1) then
    call RecvMsgFromRight(neighbors(RIGHT), halfLength)
    call SendMsgToRight(pcurrent, halfLength, neighbors(RIGHT), lower, upperx, uppery, upperz)
endif

end subroutine SendRecvMsgToNeighbors



! ============================================================================
! Subroutine    : SendMsgToTop
!
! Description   :
! ============================================================================
subroutine SendMsgToTop(pcurrent, halfLength, dest, lower, upperx, uppery, upperz)

integer                                                 :: bufcount, error, halfLength, dest
integer                                                 :: lower, upperx, uppery, upperz
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: pcurrent

call CopyPressureToBuffer(1, nxlocal, 1, nylocal, 1, halfLength, pcurrent, sendBuffTop, &
                            lower, upperx, uppery, upperz)

countRequest = countRequest + 1
bufcount = nxlocal * nylocal * halfLength

Call MPI_Isend(sendBuffTop, bufcount, MPI_REAL, dest, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'SendMsgToTop: ', dest
#endif

end subroutine SendMsgToTop



! ============================================================================
! Subroutine    : RecvMsgFromTop
!
! Description   :
! ============================================================================
subroutine RecvMsgFromTop(ori, halfLength)

integer                 :: bufcount, error, ori, halfLength

countRequest = countRequest + 1
bufcount = nxlocal * nylocal * halfLength

Call MPI_Irecv(recvBuffTop, bufcount, MPI_REAL, ori, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Waiting msg from Top: ', ori
#endif

end subroutine RecvMsgFromTop



! ============================================================================
! Subroutine    : SendMsgToBottom
!
! Description   :
! ============================================================================
subroutine SendMsgToBottom(pcurrent, halfLength, dest, lower, upperx, uppery, upperz)

integer                                                 :: bufcount, error, halfLength, dest
integer                                                 :: lower, upperx, uppery, upperz
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: pcurrent

call CopyPressureToBuffer(1, nxlocal, 1, nylocal, (nzlocal-halfLength+1), nzlocal, pcurrent, sendBuffBot, &
                            lower, upperx, uppery, upperz)

countRequest = countRequest + 1
bufcount = nxlocal * nylocal * halfLength

Call MPI_Isend(sendBuffBot, bufcount, MPI_REAL, dest, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'SendMsgToBottom: ', dest
#endif

end subroutine SendMsgToBottom



! ============================================================================
! Subroutine    : RecvMsgFromBottom
!
! Description   :
! ============================================================================
subroutine RecvMsgFromBottom(ori, halfLength)

integer                 :: bufcount, error, ori, halfLength

countRequest = countRequest +1
bufcount = nxlocal * nylocal * halfLength

Call MPI_Irecv(recvBuffBot, bufcount, MPI_REAL, ori, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Waiting msg from Bottom: ', ori
#endif

end subroutine RecvMsgFromBottom



! ============================================================================
! Subroutine    : SendMsgToBack
!
! Description   :
! ============================================================================
subroutine SendMsgToBack(pcurrent, halfLength, dest, lower, upperx, uppery, upperz)

integer                                                 :: bufcount, error, halfLength, dest
integer                                                 :: lower, upperx, uppery, upperz
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: pcurrent

call CopyPressureToBuffer(1, nxlocal, 1, halfLength, 1, nzlocal, pcurrent, sendBuffBack, &
                            lower, upperx, uppery, upperz)

countRequest = countRequest + 1
bufcount = nxlocal * halfLength * nzlocal

Call MPI_Isend(sendBuffBack, bufcount, MPI_REAL, dest, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'SendMsgToBack: ', dest
#endif

end subroutine SendMsgToBack



! ============================================================================
! Subroutine    : RecvMsgFromBack
!
! Description   :
! ============================================================================
subroutine RecvMsgFromBack(ori, halfLength)

integer                 :: bufcount, error, ori, halfLength

countRequest = countRequest + 1
bufcount = nxlocal * halfLength * nzlocal

Call MPI_Irecv(recvBuffBack, bufcount, MPI_REAL, ori, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Waiting msg from Back: ', ori
#endif

end subroutine RecvMsgFromBack



! ============================================================================
! Subroutine    : SendMsgToFront
!
! Description   :
! ============================================================================
subroutine SendMsgToFront(pcurrent, halfLength, dest, lower, upperx, uppery, upperz)

integer                                                 :: bufcount, error, halfLength, dest
integer                                                 :: lower, upperx, uppery, upperz
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: pcurrent

call CopyPressureToBuffer(1, nxlocal, (nylocal-halfLength+1), nylocal, 1, nzlocal, pcurrent, sendBuffFront, &
                            lower, upperx, uppery, upperz)

countRequest = countRequest + 1
bufcount = nxlocal * halfLength * nzlocal

Call MPI_Isend(sendBuffFront, bufcount, MPI_REAL, dest, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'SendMsgToFront: ', dest
#endif

end subroutine SendMsgToFront



! ============================================================================
! Subroutine    : RecvMsgFromFront
!
! Description   :
! ============================================================================
subroutine RecvMsgFromFront(ori, halfLength)

integer                 :: bufcount, error, ori, halfLength

countRequest = countRequest + 1
bufcount = nxlocal * halfLength * nzlocal

Call MPI_Irecv(recvBuffFront, bufcount, MPI_REAL, ori, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Waiting msg from Front: ', ori
#endif

end subroutine RecvMsgFromFront



! ============================================================================
! Subroutine    : SendMsgToLeft
!
! Description   :
! ============================================================================
subroutine SendMsgToLeft(pcurrent, halfLength, dest, lower, upperx, uppery, upperz)

integer                                                 :: bufcount, error, halfLength, dest
integer                                                 :: lower, upperx, uppery, upperz
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: pcurrent

call CopyPressureToBuffer(1, halfLength, 1, nylocal, 1, nzlocal, pcurrent, sendBuffLeft, &
                            lower, upperx, uppery, upperz)

countRequest = countRequest + 1
bufcount = halfLength * nylocal * nzlocal
Call MPI_Isend(sendBuffLeft, bufcount, MPI_REAL, dest, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'SendMsgToLeft: ', dest
#endif

end subroutine SendMsgToLeft



! ============================================================================
! Subroutine    : RecvMsgFromLeft
!
! Description   :
! ============================================================================
subroutine RecvMsgFromLeft(ori, halfLength)

integer                 :: bufcount, error, ori, halfLength

countRequest = countRequest + 1
bufcount = halfLength * nylocal * nzlocal

Call MPI_Irecv(recvBuffLeft, bufcount, MPI_REAL, ori, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Waiting msg from Left: ', ori
#endif

end subroutine RecvMsgFromLeft



! ============================================================================
! Subroutine    : SendMsgToRight
!
! Description   :
! ============================================================================
subroutine SendMsgToRight(pcurrent, halfLength, dest, lower, upperx, uppery, upperz)

integer                                                 :: bufcount, error, halfLength, dest
integer                                                 :: lower, upperx, uppery, upperz
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: pcurrent

call CopyPressureToBuffer((nxlocal-halfLength+1), nxlocal, 1, nylocal, 1, nzlocal, pcurrent, sendBuffRight, &
                            lower, upperx, uppery, upperz)

countRequest = countRequest + 1
bufcount = halfLength * nylocal * nzlocal

Call MPI_Isend(sendBuffRight, bufcount, MPI_REAL, dest, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'SendMsgToRight: ', dest
#endif

end subroutine SendMsgToRight



! ============================================================================
! Subroutine    : RecvMsgFromFront
!
! Description   :
! ============================================================================
subroutine RecvMsgFromRight(ori, halfLength)

integer                 :: bufcount, error, ori, halfLength

countRequest = countRequest + 1
bufcount = halfLength * nylocal * nzlocal

Call MPI_Irecv(recvBuffRight, bufcount, MPI_REAL, ori, 1, MPI_COMM_WORLD, request(countRequest), error)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Waiting msg from Right: ', ori
#endif

end subroutine RecvMsgFromRight



! ============================================================================
! Subroutine    : CopyPressureToBuffer
!
! Description   :
! ============================================================================
subroutine CopyPressureToBuffer(xi, xf, yi, yf, zi, zf, pressure, buffer, lower, upx, upy, upz)

integer                                         :: i, j, k, x, y, z
integer                                         :: xi, xf, yi, yf, zi, zf
integer                                         :: lower, upx, upy, upz
real,dimension(:,:,:)                           :: buffer
real,dimension(lower:upx,lower:upy,lower:upz)   :: pressure

z = 1
do k=zi, zf
    y = 1
    do j=yi, yf
        x = 1
        do i=xi, xf
            buffer(x,y,z) = pressure(i,j,k)
            x = x + 1
        enddo
        y = y + 1
    enddo
    z = z + 1
enddo

end subroutine CopyPressureToBuffer



! ============================================================================
! Subroutine    : CheckComunicationComplete
!
! Description   :
! ============================================================================
subroutine UnpackMsgReceived(pcurrent, halfLength, lower, upperx, uppery, upperz)

integer                                                 :: neighbor, halfLength
integer                                                 :: lower, upperx, uppery, upperz
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: pcurrent

if (neighbors(TOP) /= -1) then
    call CopyBufferToPressure(1, nxlocal, 1, nylocal, (1-halfLength), 0, &
                                lower, upperx, uppery, upperz, pcurrent, recvBuffTop)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Reveived msg from Top: ', neighbors(TOP)
#endif
endif


if (neighbors(BOTTOM) /= -1) then
    call CopyBufferToPressure(1, nxlocal, 1, nylocal, (nzlocal+1), (nzlocal+halfLength), &
                                lower, upperx, uppery, upperz, pcurrent, recvBuffBot)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Reveived msg from Bottom: ', neighbors(BOTTOM)
#endif
endif


if (neighbors(BACK) /= -1) then
    call CopyBufferToPressure(1, nxlocal, (1-halfLength), 0, 1, nzlocal, &
                                lower, upperx, uppery, upperz, pcurrent, recvBuffBack)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Reveived msg from Back: ', neighbors(BACK)
#endif
endif


if (neighbors(FRONT) /= -1) then
    call CopyBufferToPressure(1, nxlocal, (nylocal+1), (nylocal+halfLength), 1, nzlocal, &
                                lower, upperx, uppery, upperz, pcurrent, recvBuffFront)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Reveived msg from Front: ', neighbors(FRONT)
#endif
endif


if (neighbors(LEFT) /= -1) then
    call CopyBufferToPressure((1-halfLength), 0, 1, nylocal, 1, nzlocal, &
                                lower, upperx, uppery, upperz, pcurrent, recvBuffLeft)

#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Reveived msg from left: ', neighbors(LEFT)
#endif
endif


if (neighbors(RIGHT) /= -1) then
    call CopyBufferToPressure((nxlocal+1), (nxlocal+halfLength), 1, nylocal, 1, nzlocal, &
                                lower, upperx, uppery, upperz, pcurrent, recvBuffRight)
#ifdef DEBUG
    write(noutLog,'(//,a,i5)') 'Reveived msg from Right: ', neighbors(RIGHT)
#endif
endif

end subroutine UnpackMsgReceived



! ============================================================================
! Subroutine    : CopyBufferToPressure
!
! Description   :
! ============================================================================
subroutine CopyBufferToPressure(xi, xf, yi, yf, zi, zf, lower, upx, upy, upz, pressure, buffer)

integer                                         :: i, j, k, x, y, z
integer                                         :: xi, xf, yi, yf, zi, zf
integer                                         :: lower, upx, upy, upz
real, dimension(:,:,:)                          :: buffer
real, dimension(lower:upx,lower:upy,lower:upz)  :: pressure

z = 1
do k=zi, zf
    y = 1
    do j=yi, yf
        x = 1
        do i=xi, xf
            pressure(i,j,k) = buffer(x,y,z)
            x = x + 1
        enddo
        y = y + 1
    enddo
    z = z + 1
enddo

end subroutine CopyBufferToPressure



! ============================================================================
! Subroutine    : DefineIndexesToComputeMsgRecv
!
! Description   :
! ============================================================================
subroutine DefineIndexesToComputeMsgRecv(nx, ny, nz, halfLength)

integer         :: nx, ny, nz, halfLength, n = 0

call DefineIndexFromTop(nx, ny, halfLength, n)

call DefineIndexFromBottom(nx, ny, halfLength, n)

call DefineIndexFromBack(nx, halfLength, n)

call DefineIndexFromFront(nx, halfLength, n)

call DefineIndexFromLeft(halfLength, n)

call DefineIndexFromRight(halfLength, n)

do n = n + 1, MAX_NEIGHBORS
    call FillIndexes(-1, -1, -1, -1, -1, -1, n)
enddo

end subroutine DefineIndexesToComputeMsgRecv



! ============================================================================
! Subroutine    : DefineIndexFromTop
!
! Description   :
! ============================================================================
subroutine DefineIndexFromTop(nx, ny, halfLength, n)

integer         :: nx, ny, halfLength, n, xi, xf, yi, yf, zi, zf

if (neighbors(TOP) /= -1) then
    xi = 1
    xf = nxlocal
    if (startx == 1) xi = halfLength + 1
    if (endx == nx) xf = nxlocal - halfLength

    yi = 1
    yf = nylocal
    if (starty == 1) yi = halfLength + 1
    if (endy == ny) yf = nylocal - halfLength

    zi = 1
    zf = halfLength
    n = n + 1
    call FillIndexes(xi, xf, yi, yf, zi, zf, n)
endif

end subroutine DefineIndexFromTop



! ============================================================================
! Subroutine    : DefineIndexFromBottom
!
! Description   :
! ============================================================================
subroutine DefineIndexFromBottom(nx, ny, halfLength, n)

integer         :: nx, ny, halfLength, n, xi, xf, yi, yf, zi, zf

if (neighbors(BOTTOM) /= -1) then
    xi = 1
    xf = nxlocal
    if (startx == 1) xi = halfLength + 1
    if (endx == nx) xf = nxlocal - halfLength

    yi = 1
    yf = nylocal
    if (starty == 1) yi = halfLength + 1
    if (endy == ny) yf = nylocal - halfLength

    zi = (nzlocal - halfLength) + 1
    zf = nzlocal

    n = n + 1
    call FillIndexes(xi, xf, yi, yf, zi, zf, n)
endif

end subroutine DefineIndexFromBottom



! ============================================================================
! Subroutine    : DefineIndexFromBack
!
! Description   :
! ============================================================================
subroutine DefineIndexFromBack(nx, halfLength, n)

integer         :: nx, halfLength, n, xi, xf, yi, yf, zi, zf

if (neighbors(BACK) /= -1) then
    xi = 1
    xf = nxlocal
    if (startx == 1) xi = halfLength + 1
    if (endx == nx) xf = nxlocal - halfLength

    yi = 1
    yf = halfLength

    zi = halfLength + 1
    zf = nzlocal - halfLength

    n = n + 1
    call FillIndexes(xi, xf, yi, yf, zi, zf, n)
endif

end subroutine DefineIndexFromBack



! ============================================================================
! Subroutine    : DefineIndexFromFront
!
! Description   :
! ============================================================================
subroutine DefineIndexFromFront(nx, halfLength, n)

integer         :: nx, halfLength, n, xi, xf, yi, yf, zi, zf

if (neighbors(FRONT) /= -1) then
    xi = 1
    xf = nxlocal
    if (startx == 1) xi = halfLength + 1
    if (endx == nx) xf = nxlocal - halfLength

    yi = (nylocal - halfLength) + 1
    yf = nylocal

    zi = halfLength + 1
    zf = nzlocal - halfLength

    n = n + 1
    call FillIndexes(xi, xf, yi, yf, zi, zf, n)
endif

end subroutine DefineIndexFromFront



! ============================================================================
! Subroutine    : DefineIndexFromLeft
!
! Description   :
! ============================================================================
subroutine DefineIndexFromLeft(halfLength, n)

integer         :: halfLength, n, xi, xf, yi, yf, zi, zf

if (neighbors(LEFT) /= -1) then
    xi = 1
    xf = halfLength

    yi = halfLength + 1
    yf = nylocal - halfLength

    zi = halfLength + 1
    zf = nzlocal - halfLength

    n = n + 1
    call FillIndexes(xi, xf, yi, yf, zi, zf, n)
endif

end subroutine DefineIndexFromLeft



! ============================================================================
! Subroutine    : DefineIndexFromRight
!
! Description   :
! ============================================================================
subroutine DefineIndexFromRight(halfLength, n)

integer         :: halfLength, n, xi, xf, yi, yf, zi, zf

if (neighbors(RIGHT) /= -1) then
    xi = (nxlocal - halfLength) + 1
    xf = nxlocal

    yi = halfLength + 1
    yf = nylocal - halfLength

    zi = halfLength + 1
    zf = nzlocal - halfLength

    n = n + 1
    call FillIndexes(xi, xf, yi, yf, zi, zf, n)
endif

end subroutine DefineIndexFromRight



! ============================================================================
! Subroutine    : FillIndexes
!
! Description   :
! ============================================================================
subroutine FillIndexes(xi, xf, yi, yf, zi, zf, n)

integer     :: xi, xf, yi, yf, zi, zf, n

indexes(n)%xi = xi
indexes(n)%xf = xf
indexes(n)%yi = yi
indexes(n)%yf = yf
indexes(n)%zi = zi
indexes(n)%zf = zf

end subroutine FillIndexes



! ============================================================================
! Subroutine    : DefineIndexesToComputeSubdomain
!
! Description   :
! ============================================================================
subroutine DefineIndexesToComputeSubdomain(xi, xf, yi, yf, zi, zf, nx, ny, nz, halfLength, boundary)

integer                 :: xi, xf, yi, yf, zi, zf
integer                 :: nx, ny, nz, halfLength
logical                 :: boundary

if (boundary) then
    xi = 1
    yi = 1
    zi = 1
    if (startx == 1) xi = halfLength + 1
    xf = nxlocal
    if (endx == nx) xf = nxlocal - halfLength

    if (starty == 1) yi = halfLength + 1
    yf = nylocal
    if (endy == ny) yf = nylocal - halfLength

    if (startz == 1) zi = halfLength + 1
    zf = nzlocal
    if (endz == nz) zf = nzlocal - halfLength
else
    xi = halfLength + 1
    xf = nxlocal - halfLength
    yi = xi
    yf = nylocal - halfLength
    zi = xi
    zf = nzlocal - halfLength
endif

end subroutine DefineIndexesToComputeSubdomain



! ============================================================================
! Subroutine    : FindProcWithObserver
!
! Description   : Identify which MPI process contains the observer.
! ============================================================================
subroutine FindProcWithObserver(planex, planey, kobs)

integer         :: planex, planey, kobs
integer         :: i, j, k, x, y, z

do i = 1, npx
    if(planex < lstartx(i)) exit
enddo
ipobs = i -1
xpobs = planex - lstartx(ipobs) + 1

do j = 1, npy
    if(planey < lstarty(j)) exit
enddo
jpobs = j -1
ypobs = planey - lstarty(jpobs) + 1

do k = 1, npz
    if(kobs < lstartz(k)) exit
enddo
kpobs = k -1
zpobs = kobs - lstartz(kpobs) + 1

#ifdef DEBUG
  write(noutLog,'(//,a)') 'Info process with observer: '
  write(noutLog,'(a)') 'Position: '
  write(noutLog,*) 'xpobs: ', xpobs
  write(noutLog,*) 'ypobs: ', ypobs
  write(noutLog,*) 'zpobs: ', zpobs
  write(noutLog,'(a)') 'Indexes: '
  write(noutLog,*) 'ipobs: ', ipobs
  write(noutLog,*) 'jpobs: ', jpobs
  write(noutLog,*) 'kpobs: ', kpobs
#endif

end subroutine FindProcWithObserver



! ============================================================================
! Subroutine    : SaveSeismogram
!
! Description   :
! ============================================================================
subroutine SaveSeismogram(seismogramx, seismogramy, ntotal)

integer                         :: ntotal, p, ierr, world_group, seismx_group
integer                         :: seismy_group, seismx_comm, seismy_comm
integer, dimension(1:npx)       :: procIdListX
integer, dimension(1:npy)       :: procIdListY
real, dimension(ntotal:nxlocal) :: seismogramx
real, dimension(ntotal:nylocal) :: seismogramy

! Seismogram X
do p = 1, npx
    procIdListX(p) = procMatrix(p,jpobs,kpobs)
enddo

call MPI_COMM_GROUP(MPI_COMM_WORLD, world_group, ierr)

call MPI_GROUP_INCL(world_group, npx, procIdListX, seismx_group, ierr)

call MPI_COMM_CREATE(MPI_COMM_WORLD, seismx_group, seismx_comm, ierr)

if (seismx_comm /= MPI_COMM_NULL) then
    !write(*,*) 'Salvando sismogra_X, procId: ', procId
    call PrintSeismogram(seismx_comm, ntotal, nxlocal, seismogramx, SEISM_FILE_X, startx)

endif


! Seismogram Y
do p = 1, npy
    procIdListY(p) = procMatrix(ipobs,p,kpobs)
enddo

call MPI_GROUP_INCL(world_group, npy, procIdListY, seismy_group, ierr)

call MPI_COMM_CREATE(MPI_COMM_WORLD, seismy_group, seismy_comm, ierr)

if (seismy_comm /= MPI_COMM_NULL) then
    !write(*,*) 'Salvando sismogra_Y, procId: ', procId
    call PrintSeismogram(seismy_comm, ntotal, nylocal, seismogramy, SEISM_FILE_Y, starty)

endif

#ifdef DEBUG
    do p = 1, npx
        if (procMatrix(p,jpobs,kpobs) == procId) then
            call PrintPartSeismogram(ntotal, nxlocal, seismogramx, SEISM_FILE_X, p)
        endif
    enddo

    do p = 1, npy
        if (procMatrix(ipobs,p,kpobs) == procId) then
            call PrintPartSeismogram(ntotal, nylocal, seismogramy, SEISM_FILE_Y, p)
        endif
    enddo
#endif

end subroutine SaveSeismogram



! ============================================================================
! Subroutine    : PrintSeismogram
!
! Description   :
! ============================================================================
subroutine PrintSeismogram(seism_comm, ntotal, nlocal, seismogram, filename, start)

integer                             :: bufsize, ierr, seism_comm
integer                             :: ntotal, nlocal, start, file
integer(kind=MPI_OFFSET_KIND)       :: disp
real, dimension(nlocal, ntotal)     :: seismogram
character(len=50)                   :: filename, fullname

fullname = trim(filename) // '.bin'
disp = (start - 1) * ntotal * 4
bufsize = nlocal * ntotal

call MPI_FILE_OPEN(seism_comm, trim(fullname), MPI_MODE_WRONLY + MPI_MODE_CREATE, &
                       MPI_INFO_NULL, file, ierr)

call MPI_FILE_SET_VIEW(file, disp, MPI_REAL, MPI_REAL, 'native', &
                           MPI_INFO_NULL, ierr)

call MPI_FILE_WRITE(file, seismogram, bufsize, MPI_REAL, MPI_STATUS_IGNORE, ierr)

call MPI_FILE_CLOSE(file, ierr)

end subroutine PrintSeismogram



! ============================================================================
! Subroutine    : DeallocateCommunicationArrays
!
! Description   : Deallocate buffers to exchange message among the MPI processes.
! ============================================================================
subroutine DeallocateCommunicationArrays()

integer                 :: status

deallocate(sendBuffTop, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate send buffer top ***'

deallocate(sendBuffBot, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate send buffer bottom ***'

deallocate(sendBuffBack, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate send buffer back ***'

deallocate(sendBuffFront, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate send buffer front ***'

deallocate(sendBuffLeft, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate send buffer left ***'

deallocate(sendBuffRight, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate send buffer right ***'


deallocate(recvBuffTop, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate recv buffer top ***'

deallocate(recvBuffBot, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate recv buffer bottom ***'

deallocate(recvBuffBack, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate recv buffer back ***'

deallocate(recvBuffFront, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate recv buffer front ***'

deallocate(recvBuffLeft, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate recv buffer left ***'

deallocate(recvBuffRight, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate recv buffer right ***'

deallocate(indexes, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate indexes array ***'

deallocate(procMatrix, stat = status)
if (status /= 0) write (*,*) '*** It is not possible deallocate matrix of processes ***'

end subroutine DeallocateCommunicationArrays





end module MPICommunication
