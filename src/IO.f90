module IO

implicit none
integer                         :: ninInputFile = 10, ninVel = 11
integer                         :: noutLog = 20, noutSeism = 50
character(len=50)               :: velFile                          ! velocity file name
character(len=50)               :: logFile                          ! log file name
character(len=50), parameter    :: SEISM_FILE_X = 'Seismogram_X'
character(len=50), parameter    :: SEISM_FILE_Y = 'Seismogram_Y'



contains
! ============================================================================
! Subroutine    : InitializeLog
!
! Description   : Initialze log file for MPI processes.
! ============================================================================
subroutine InitializeLog(procId)

character(len=3)        :: srank
integer                 :: i, nx, ny, nz, procId

write(srank(1:3),'(i3.3)') procId
logFile = 'log_rank_'//srank//'.dat'

open (unit = noutLog, file = trim(logFile) )

end subroutine InitializeLog



! ============================================================================
! Subroutine    : LoadParemeters
!
! Description   : Load parameters of the seismic modeling from input file.
! ============================================================================
subroutine LoadParemeters(nx, ny, nz, h, dt, ntotal, fcutoff, kobs)

integer             :: nx, ny, nz, ntotal, kobs
real                :: h, dt, fcutoff
character(len=50)   :: inputFile

call getarg(1,inputFile)

if ( inputFile == '' ) then
    stop 'No input file.'
endif

open(unit = ninInputFile,file = inputFile)
read(ninInputFile,*) nx, ny, nz
read(ninInputFile,*) h, dt, ntotal
read(ninInputFile,*) fcutoff, kobs
read(ninInputFile,*) velFile

#ifdef DEBUG
    write(noutLog,'(//,a)') 'Parameters loaded:'
    write(noutLog,*) 'Nx: ',Nx,', Ny: ',Ny,', Nz: ',Nz
    write(noutLog,*) 'h: ',h,', dt: ',dt,', NTotal: ',NTotal
    write(noutLog,*) 'fcutoff: ',fcutoff,', kobs: ',kobs
    write(noutLog,*) 'velFile: ', velFile
#endif

end subroutine LoadParemeters



! ============================================================================
! Subroutine    : ReadVelocityModel
!
! Description   : Read velocity model from file.
! ============================================================================
subroutine ReadVelocityModel(nx, ny, nz, velGlobal)

integer                         :: i, j, k, reclength, ierr
integer                         :: nx, ny, nz
real, dimension(nx, ny, nz)     :: velGlobal

inquire (iolength=reclength) velGlobal
open(UNIT=ninVel, FILE=velFile, status='old', form='UNFORMATTED', access='direct', action='read', recl=reclength)
read(UNIT=ninVel, REC=1, IOSTAT=ierr) (((VelGlobal(i,j,k), k=1, nz), j=1, ny), i=1, nx)

if(ierr /= 0) then
    write(*,*) 'Error in reading from velocity file.', ierr
    stop
endif

close(ninVel)

end subroutine ReadVelocityModel



! ============================================================================
! Subroutine    : PrintArrayAllocation
!
! Description   :
! ============================================================================
subroutine PrintArrayAllocation(start, endx, endy, endz, nxlocal, nylocal, nzlocal, ntotal)

integer         :: start, endx, endy, endz
integer         :: nxlocal, nylocal, nzlocal, ntotal

write(noutLog,'(//,a)') 'Arrays allocated:'
write(noutLog,*) '# pprev, pnext: '
write(noutLog,*) 'x: ', start, ' - ',endx
write(noutLog,*) 'y: ', start, ' - ',endy
write(noutLog,*) 'z: ', start, ' - ',endz
write(noutLog,*) '# vel, C: '
write(noutLog,*) 'x: 1 - ',nxlocal
write(noutLog,*) 'y: 1 - ',nylocal
write(noutLog,*) 'z: 1 - ',nzlocal
write(noutLog,*) '# seismogram: '
write(noutLog,*) 'x: 1 - ',nxlocal
write(noutLog,*) 'y: 1 - ',nylocal
write(noutLog,*) 'z: 1 - ',ntotal

end subroutine PrintArrayAllocation



! ============================================================================
! Subroutine    : ClearSeismogramFile
!
! Description   :
! ============================================================================
subroutine ClearSeismogramFile(filename)

character(len=50)                   :: filename, fullname

fullname = trim(filename) // '.bin'
open(unit=noutSeism, file=fullname, status="replace")
close(noutSeism)

end subroutine ClearSeismogramFile



! ============================================================================
! Subroutine    : PrintVelSubArray
!
! Description   :
! ============================================================================
subroutine PrintVelSubArray(vel,nxlocal, nylocal, nzlocal)

integer                 :: nxlocal, nylocal, nzlocal, k
real, dimension(:,:,:)  :: vel

write(noutLog,'(//,a)') 'Vel subarray values:'
do k=1,nzlocal
    write(noutLog,*) "vel(1,1,", k,"): ", vel(1,1,k)
enddo

do k=1,nzlocal
    write(noutLog,*) "vel(nxlocal,nylocal,",k,"): ", vel(nxlocal,nylocal,k)
enddo

end subroutine PrintVelSubArray



! ============================================================================
! Subroutine    : PrintZBuffer
!
! Description   :
! ============================================================================
subroutine PrintZBuffer(buffer, startz, endz, lower, upperx, uppery, upperz)

integer                                                 :: k, startz, endz
integer                                                 :: lower, upperx, uppery, upperz
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: buffer

do k = startz, endz
    write(noutLog,*) "value(1,1,",k,"): ", buffer(1,1,k)
enddo


end subroutine PrintZBuffer



! ============================================================================
! Subroutine    : PrintYBuffer
!
! Description   :
! ============================================================================
subroutine PrintYBuffer(buffer, starty, endy, lower, upperx, uppery, upperz)

integer                                                 :: j, starty, endy
integer                                                 :: lower, upperx, uppery, upperz
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: buffer

do j = starty, endy
    write(noutLog,*) 'value(1,', j, ',1): ', buffer(1,j,1)
enddo


end subroutine PrintYBuffer



! ============================================================================
! Subroutine    : PrintXBuffer
!
! Description   :
! ============================================================================
subroutine PrintXBuffer(buffer, startx, endx, lower, upperx, uppery, upperz)

integer                                                 :: i, startx, endx
integer                                                 :: lower, upperx, uppery, upperz
real, dimension(lower:upperx,lower:uppery,lower:upperz) :: buffer

do i = startx, endx
    write(noutLog,*) 'value(',i,',1,1): ', buffer(i,1,1)
enddo


end subroutine PrintXBuffer




! ============================================================================
! Subroutine    : PrintPartSeismogram
!
! Description   :
! ============================================================================
subroutine PrintPartSeismogram(ntotal, nlocal, seismogram, filename, start)

integer                             :: i, k, ntotal, nlocal, start
real, dimension(ntotal, nlocal)     :: seismogram
character(len=50)                   :: filename, fullname, suffix

write(suffix, '(a,i5.5)') '_',start
fullname = trim(filename) // trim(suffix) // '.dat'

open (unit = noutSeism, file = fullname)
write(noutSeism,'(E14.7)') ((seismogram(k,i), k=1, ntotal), i=1, nlocal)
close(noutSeism)

end subroutine PrintPartSeismogram



end module IO
