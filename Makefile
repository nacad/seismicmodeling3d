# ============================================================================
# Name        : Makefile
# Author      : 
# Version     :
# Copyright   : Your copyright notice
# Description : Makefile for SeismicModeling in Fortran
# ============================================================================

.PHONY: all clean

all: 
	mpif90 -cpp -DDEBUG -fopenmp -O3 -g -o bin/SeismicModeling \
		src/IO.f90 src/GlobalDataTypes.f90 src/WaveEquation.f90 src/MPICommunication.f90 src/SeismicModeling.f90

clean:
	rm -f bin/SeismicModeling *.mod
