# Seismic Modeling 3D #

Accurate modeling of seismic wave propagation in complex media is extremely
important for many seismic processing algorithms. These algorithms are computationally
expensive, so they need scalable parallel systems to produce results in
a timely manner. We implements a parallel acoustic 
wave propagation model, that employs the Finite Difference Method (FDM) with 8th-order 
in space and second order in time. The implementation uses hybrid MPI/OpenMP where the 
domain decomposition is performed via MPI and OpenMP is used to parallelize the stencil 
computation in each MPI process.